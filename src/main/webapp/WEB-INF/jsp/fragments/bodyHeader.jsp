<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<nav class="navbar navbar-dark bg-dark py-0">
    <div class="container">
        <sec:authorize access="isAuthenticated()">
            <a href="index" class="navbar-brand">ДОМ</a>
            <form:form class="form-inline my-2" action="logout" method="post">
                <sec:authorize access="hasRole('ADMIN')">
                    <a class="btn btn-info mr-1" href="projects">Проекты</a>
                </sec:authorize>
                <a class="btn btn-info mr-1" href="tasks">Задачи</a>
                <button class="btn btn-primary my-1" type="submit">
                    <span class="fa fa-sign-out">Log out</span>
                </button>
            </form:form>
        </sec:authorize>
        <sec:authorize access="isAnonymous()">
            <form:form class="form-inline my-2" id="login_form" action="spring_security_check" method="post">
                <input class="form-control mr-1" type="text" placeholder="Email" name="email">
                <input class="form-control mr-1" type="password" placeholder="Password" name="password">
                <button class="btn btn-success" type="submit">
                    <span class="fa fa-sign-in">Sign in</span>
                </button>
            </form:form>
        </sec:authorize>
    </div>
</nav>
