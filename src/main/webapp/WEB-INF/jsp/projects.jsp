<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.04.2020
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://topjava.javawebinar.ru/functions" %>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/bodyHeader.jsp"/>

<section>
    <h3>Проекты</h3>
    <hr>
    <a href="projects/create">Создать проект</a>
    <hr>
    <table border="1" cellpadding="8" cellspacing="0">
        <thead>
        <tr>
            <th>Имя проекта</th>
            <th>Дата начала</th>
            <th>Дата завершения</th>
            <th>Задачи проекта</th>
            <th>Редактировать</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <c:forEach items="${projects}" var="project">
            <tr>
                <td>${project.name}</td>
                <td>${fn:forTable(project.dateBegin)}</td>
                <td>${fn:forTable(project.dateEnd)}</td>
                <td><a href="tasks/projectTasks?projectId=${project.id}">Задачи</a></td>
                <td><a href="projects/update?id=${project.id}">Редактировать</a></td>
                <td><a href="projects/delete?id=${project.id}">Удалить</a></td>
            </tr>
        </c:forEach>
    </table>
</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
