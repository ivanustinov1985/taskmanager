<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.04.2020
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://topjava.javawebinar.ru/functions" %>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/bodyHeader.jsp"/>

<section>
    <hr>
    <form method="post" action="projects">
        <input type="hidden" name="id" value="${project.id}">
        <dl>
            <dt>Имя проекта</dt>
            <dd><input type="text" value="${project.name}" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Дата начала</dt>
            <dd><input type="date" value="${fn:forForm(project.dateBegin)}" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>Дата конца</dt>
            <dd><input type="date" value="${fn:forForm(project.dateEnd)}" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>
</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
