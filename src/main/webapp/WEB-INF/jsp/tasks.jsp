<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.04.2020
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://topjava.javawebinar.ru/functions" %>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/bodyHeader.jsp"/>

<section>
    <h3>Задачи</h3>
    <hr>
    <a href="tasks/create?projectId=${projectId}">Создать задачу</a>
    <hr>
    <table border="1" cellpadding="8" cellspacing="0">
        <thead>
        <tr>
            <th>Имя задачи</th>
            <th>Дата начала</th>
            <th>Дата завершения</th>
<%--            <th>Проект</th>--%>
            <th>Редактировать</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <c:forEach items="${tasks}" var="task">
            <tr>
                <td>${task.name}</td>
                <td>${fn:forTable(task.dateBegin)}</td>
                <td>${fn:forTable(task.dateEnd)}</td>
<%--                <td>${task.project.name}</td>--%>
                <td><a href="tasks/update?id=${task.id}">Редактировать</a></td>
                <td><a href="tasks/delete?id=${task.id}">Удалить</a></td>
            </tr>
        </c:forEach>
    </table>
</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
