<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 23.04.2020
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://topjava.javawebinar.ru/functions" %>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/bodyHeader.jsp"/>

<section>
    <hr>
    <form method="post" action="tasks">
        <input type="hidden" name="projectId" value="${task.project.id != null ? task.project.id : projectId}">
        <input type="hidden" name="id" value="${task.id}">
        <dl>
            <dt>Имя задачи</dt>
            <dd><input type="text" value="${task.name}" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Дата начала</dt>
            <dd><input type="date" value="${fn:forForm(task.dateBegin)}" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>Дата конца</dt>
            <dd><input type="date" value="${fn:forForm(task.dateEnd)}" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>
</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
