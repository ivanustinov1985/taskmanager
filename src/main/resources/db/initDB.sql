DROP TABLE IF EXISTS projects;
DROP TABLE if EXISTS tasks;
DROP TABLE if EXISTS user_roles;
DROP TABLE if EXISTS users;

CREATE TABLE IF NOT EXISTS projects
(
    id         VARCHAR(255) NOT NULL,
    name       VARCHAR(255) NOT NULL,
    date_begin DATE,
    date_end   DATE,
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS tasks
(
    id         VARCHAR(255) NOT NULL,
    project_id VARCHAR(255),
    name       VARCHAR(255) NOT NULL,
    date_begin DATE,
    date_end   DATE,
    PRIMARY KEY (id),
    foreign key (project_id) references projects (id)
);

CREATE TABLE IF NOT EXISTS users
(
    id       VARCHAR(255) NOT NULL,
    name     VARCHAR(255) NOT NULL,
    email    VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    enabled  BOOLEAN      NOT NULL,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX users_unique_email_idx ON users (email);

CREATE TABLE user_roles
(
    user_id VARCHAR(255) NOT NULL,
    role    VARCHAR(255) NOT NULL,
    CONSTRAINT user_roles_idx UNIQUE (user_id, role),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);