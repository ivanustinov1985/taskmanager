DELETE
FROM tasks;
DELETE
FROM projects;
DELETE
FROM user_roles;
DELETE
FROM users;

INSERT INTO projects
VALUES ('26f8ba1f-882c-11ea-8b4a-20cf3044b634', 'First project', '2019-05-30', '2019-06-30'),
       ('26f8bbae-882c-11ea-8b4a-20cf3044b634', 'Second project', '2020-01-30', '2020-02-26'),
       ('402880e571ba3f3f0171ba75b4620000', 'Third project', '2020-04-02', '2020-04-05');

INSERT INTO tasks
VALUES ('402880e571ba849e0171ba95c19c0000', '26f8ba1f-882c-11ea-8b4a-20cf3044b634', 'Task_FP', '2020-04-26',
        '2020-04-30'),
       ('402880e571ba9a530171ba9dddf90000', '26f8ba1f-882c-11ea-8b4a-20cf3044b634', 'Task_FP', '2020-04-26',
        '2020-04-29'),
       ('402880e571bb7d710171bb8613900000', null, 'Task_SP', '2020-04-02', '2020-04-05');

INSERT INTO users
VALUES ('8ccdbf6f-8966-11ea-8b4a-20cf3044b634', 'Ivan', 'ivan@yandex.ru', '{noop}pass', true),
       ('8ccdc7de-8966-11ea-8b4a-20cf3044b634', 'Petr', 'petr@yandex.ru', '{noop}password', true),
       ('8ccdc9a1-8966-11ea-8b4a-20cf3044b634', 'Constantin', 'constantin@yandex.ru', '{noop}const', false);

INSERT INTO user_roles
VALUES ('8ccdbf6f-8966-11ea-8b4a-20cf3044b634', 'ADMIN'),
       ('8ccdc7de-8966-11ea-8b4a-20cf3044b634', 'USER'),
       ('8ccdc9a1-8966-11ea-8b4a-20cf3044b634', 'USER');

