package ru.volnenko.se.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 23.04.2020
 */
public class DateTimeUtil {
    private static final SimpleDateFormat TABLE = new SimpleDateFormat("dd.MM.yyyy");
    private static final SimpleDateFormat FORM = new SimpleDateFormat("yyyy-MM-dd");


    public static String forMainTable(Date ldt) {
        return ldt == null ? "" : TABLE.format(ldt);
    }

    public static String forForm(Date ldt) {
        return ldt == null ? "" : FORM.format(ldt);
    }

}
