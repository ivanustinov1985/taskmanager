package ru.volnenko.se.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 21.04.2020
 */
public class JacksonObjectMapper extends ObjectMapper {

    private static final ObjectMapper MAPPER = new JacksonObjectMapper();

    private JacksonObjectMapper() {
        registerModule(new Hibernate5Module());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        this.setDateFormat(df);

//        registerModule(new JavaTimeModule());
//        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }


    public static ObjectMapper getMapper() {
        return MAPPER;
    }

    public static void main(String[] args) throws JsonProcessingException, ParseException {
        ObjectMapper mapper = getMapper();
        Date date = java.sql.Date.valueOf("2020-04-05");
        System.out.println(date);
        String string = mapper.writeValueAsString(date);
        System.out.println(string);
        System.out.println(mapper.readValue(string, Date.class));
        System.out.println(mapper.readValue(string, Date.class).equals(date));
    }
}
