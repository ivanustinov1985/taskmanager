package ru.volnenko.se.entity;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Denis Volnenko
 */

@Entity
@Table(name = "projects")
public final class Project extends AbstractEntity implements Serializable {

    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
    private Date dateBegin;

    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
    private Date dateEnd;

    public Project() {
    }

    public Project(String id, String name, Date dateBegin, Date dateEnd) {
        super(id, name);
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public boolean isNew() {
        return this.id == null;
    }

}
