package ru.volnenko.se.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Denis Volnenko
 */
@Entity
@Table(name = "tasks")
public final class Task extends AbstractEntity implements Serializable {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Project project;

    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public Task() {
    }

    public Task(String id, String name, Date dateBegin, Date dateEnd) {
        super(id, name);
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    public Task(String id, Project project, String name, Date dateBegin, Date dateEnd) {
        this(id, name, dateBegin, dateEnd);
        this.project = project;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name=" + name +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                "}";
    }
}
