package ru.volnenko.se.datajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.entity.Task;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */

@Repository
public final class TaskRepository implements ITaskRepository {

    @Autowired
    private CrudTaskRepository crudRepository;

    @Override
    public Task createTask(final Task task) {
        return merge(task);
    }

    @Override
    public Task getTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return crudRepository.findById(id).orElse(null);
    }

    @Override
    public List<Task> getPtojectTasks(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return crudRepository.getProjectTasks(projectId);
    }

    @Override
    public Task getByOrderIndex(final Integer orderIndex) {
        if (orderIndex == null) return null;
        return null;
    }

    @Override
    public void merge(final Task... tasks) {
        for (final Task task : tasks) merge(task);
    }

    @Override
    public void merge(final Collection<Task> tasks) {
        for (final Task task : tasks) merge(task);
    }

    @Override
    public void load(final Collection<Task> tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(final Task... tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public Task merge(final Task task) {
        if (task == null) return null;
        return crudRepository.save(task);
    }

    @Override
    public void removeTaskById(final String id) {
        if (id == null || id.isEmpty()) return;
        crudRepository.deleteById(id);
    }

    @Override
    public void removeTaskByOrderIndex(final Integer orderIndex) {
        Task task = getByOrderIndex(orderIndex);
        if (task == null) return;
        removeTaskById(task.getId());
    }

    @Override
    public List<Task> getListTask() {
        return crudRepository.findAll();
    }

    @Override
    public void clear() {
        crudRepository.deleteAll();
    }

}
