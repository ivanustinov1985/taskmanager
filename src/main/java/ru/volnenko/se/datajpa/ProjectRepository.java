package ru.volnenko.se.datajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.entity.Project;

import java.util.*;

/**
 * @author Denis Volnenko
 */

@Repository
public final class ProjectRepository implements IProjectRepository {

    @Autowired
    private CrudProjectRepository crudRepository;

    @Override
    public Project createProject(final Project project) {
        return crudRepository.save(project);
    }

    @Override
    public Project merge(final Project project) {
        if (project == null) return null;
        return crudRepository.save(project);
    }

    @Override
    public void merge(final Collection<Project> projects) {
        if (projects == null) return;
        for (final Project project: projects) merge(project);
    }

    @Override
    public void merge(final Project... projects) {
        if (projects == null) return;
        for (final Project project: projects) merge(project);
    }

    @Override
    public void load(final Collection<Project> projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(final Project... projects) {
        clear();
        merge(projects);
    }

    @Override
    public Project getProjectById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return crudRepository.findById(id).orElse(null);
    }

    @Override
    public Project removeByOrderIndex(Integer orderIndex) {
        return null;
    }

    @Override
    public void removeProjectById(final String id) {
        if (id == null || id.isEmpty()) return;
        crudRepository.delete(id);
    }

    @Override
    public List<Project> getListProject() {
        return crudRepository.findAll();
    }

    @Override
    public void clear() {
        crudRepository.deleteAll();
    }

}
