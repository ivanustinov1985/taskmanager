package ru.volnenko.se.datajpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.volnenko.se.entity.Task;

import java.util.List;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 22.04.2020
 */
public interface CrudTaskRepository extends JpaRepository<Task, String> {

    @Query("SELECT t FROM Task t WHERE t.project.id=:projectId")
    List<Task> getProjectTasks(@Param("projectId") String projectId);
}
