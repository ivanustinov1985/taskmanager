package ru.volnenko.se.datajpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.entity.Project;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 22.04.2020
 */
@Transactional(readOnly = true)
public interface CrudProjectRepository extends JpaRepository<Project, String> {

    @Transactional
    @Modifying
    @Query("DELETE FROM Project p WHERE p.id=:id")
    int delete(@Param("id") String id);
}
