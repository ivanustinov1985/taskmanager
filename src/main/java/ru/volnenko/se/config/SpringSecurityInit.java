package ru.volnenko.se.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 28.04.2020
 */

public class SpringSecurityInit extends AbstractSecurityWebApplicationInitializer {
}
