package ru.volnenko.se.controller.task;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.volnenko.se.controller.project.RestProjectController;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.net.URI;
import java.util.List;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 27.04.2020
 */
@RestController
@RequestMapping(value = RestTaskController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class RestTaskController extends AbstractTaskController{
    static final String REST_URL = "/rest/tasks";

    @Override
    @GetMapping("/{id}")
    public Task get(@PathVariable String id) {
        Task task = super.get(id);
        System.out.println(task.getDateBegin());
//        return super.get(id);
        return task;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        super.delete(id);
    }

    @Override
    @GetMapping
    public List<Task> getAll() {
        return super.getAll();
    }

    @Override
    @GetMapping("/projectTasks")
    public List<Task> getProjectTasks(@RequestParam String projectId) {
        return super.getProjectTasks(projectId);
    }

    @Override
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void update(@RequestBody Task task, @PathVariable String id) {
        super.update(task, id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> createWithLocation(@RequestBody Task task, @RequestParam String projectId) {
        Task created = super.create(task, projectId);
        URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(REST_URL + "/{id}")
                .buildAndExpand(created.getId()).toUri();

        return ResponseEntity.created(uriOfNewResource).body(created);
    }
}
