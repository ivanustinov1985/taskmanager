package ru.volnenko.se.controller.task;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.volnenko.se.entity.Task;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 22.04.2020
 */

@Controller
@RequestMapping(value = "/tasks")
public class JspTaskController extends AbstractTaskController {


    @GetMapping("/delete")
    public String delete(HttpServletRequest request) {
        super.delete(getId(request));
        return "redirect:/tasks";
    }

    @GetMapping("/update")
    public String update(HttpServletRequest request, Model model) {
        model.addAttribute("task", super.get(getId(request)));
        return "taskForm";
    }

    @GetMapping("/create")
    public String create(HttpServletRequest request, Model model) {
        model.addAttribute("projectId", request.getParameter("projectId"));
        return "taskForm";
    }

    @GetMapping("/projectTasks")
    public String getProjectTasks(HttpServletRequest request, Model model) {
        List<Task> tasks = super.getProjectTasks(request.getParameter("projectId"));
        model.addAttribute("tasks", tasks);
        model.addAttribute("projectId", request.getParameter("projectId"));
        return "tasks";
    }

    @PostMapping
    public String updateOrCreate(HttpServletRequest request) {
        Task task = new Task();
        task.setName(request.getParameter("name"));
        task.setDateBegin(Date.valueOf(request.getParameter("dateBegin")));
        task.setDateEnd(Date.valueOf(request.getParameter("dateEnd")));
        String projectId = request.getParameter("projectId");
        String id = request.getParameter("id");
        if (id == null || id.isEmpty()) {
            super.create(task, projectId);
        } else {
            super.update(task, projectId);
        }
        return "redirect:/tasks";
    }

    private String getId(HttpServletRequest request) {
        return Objects.requireNonNull(request.getParameter("id"));
    }
}
