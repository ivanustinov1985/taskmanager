package ru.volnenko.se.controller.task;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.service.TaskService;

import java.util.List;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 27.04.2020
 */
public abstract class AbstractTaskController {

    @Autowired
    private TaskService taskService;

    public void delete(final String id) {
        taskService.removeTaskById(id);
    }


    public void update(Task task, String id) {
        if (task.isNew()) {
            task.setId(id);
        } else if (!task.getId().equals(id)) {
            throw new IllegalArgumentException(task.getName() + " must be with id= " + id);
        }
        taskService.merge(task);
    }

    public Task create(Task task, String projectId) {
        return taskService.createTask(task, projectId);
    }


    public Task get(final String id) {
        return taskService.getTaskById(id);
    }

    public List<Task> getProjectTasks(String projectId) {
        return taskService.getProjectTasks(projectId);
    }

    public List<Task> getAll() {
        return taskService.getListTask();
    }
}
