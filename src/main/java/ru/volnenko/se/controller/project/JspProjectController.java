package ru.volnenko.se.controller.project;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.volnenko.se.entity.Project;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.Objects;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 27.04.2020
 */
@Controller
@RequestMapping(value = "/projects")
public class JspProjectController extends AbstractProjectController {

    @GetMapping("/delete")
    public String delete(HttpServletRequest request) {
        super.delete(getId(request));
        return "redirect:/projects";
    }

    @GetMapping("/update")
    public String update(HttpServletRequest request, Model model) {
        model.addAttribute("project", super.get(request.getParameter("projectId")));
        return "projectForm";
    }

    @GetMapping("/create")
    public String create() {
        return "projectForm";
    }

    @PostMapping
    public String updateOrCreate(HttpServletRequest request) {
        Project project = new Project();
        project.setName(request.getParameter("name"));
        project.setDateBegin(Date.valueOf(request.getParameter("dateBegin")));
        project.setDateEnd(Date.valueOf(request.getParameter("dateEnd")));
        String id = request.getParameter("id");
        if (id == null || id.isEmpty()) {
            super.create(project);
        } else {
            super.update(project, id);
        }
        return "redirect:/projects";
    }

    private String getId(HttpServletRequest request) {
        return Objects.requireNonNull(request.getParameter("id"));
    }
}
