package ru.volnenko.se.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.service.ProjectService;

import java.util.List;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 27.04.2020
 */
public abstract class AbstractProjectController {

    @Autowired
    private ProjectService projectService;


    public void delete(final String id) {
        projectService.removeProjectById(id);
    }


    public void update(Project project, String id) {
        if (project.isNew()) {
            project.setId(id);
        } else if (!project.getId().equals(id)) {
            throw new IllegalArgumentException(project.getName() + " must be with id= " + id);
        }
        projectService.merge(project);
    }

    public Project create(Project project) {
        return projectService.createProject(project);
    }


    public Project get(final String id) {
        return projectService.getProjectById(id);
    }

    public List<Project> getAll() {
        return projectService.getListProject();
    }

}
