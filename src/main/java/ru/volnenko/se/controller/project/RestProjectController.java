package ru.volnenko.se.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.service.ProjectService;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 22.04.2020
 */

@RestController
@RequestMapping(value = RestProjectController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class RestProjectController extends AbstractProjectController{
    static final String REST_URL = "/rest/projects";


    @Override
    @GetMapping("/{id}")
    public Project get(@PathVariable String id) {
        return super.get(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        super.delete(id);
    }

    @Override
    @GetMapping
    public List<Project> getAll() {
        return super.getAll();
    }

    @Override
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void update(@RequestBody Project project, @PathVariable String id) {
        super.update(project, id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> createWithLocation(@RequestBody Project project) {
        Project created = super.create(project);

        URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(REST_URL + "/{id}")
                .buildAndExpand(created.getId()).toUri();

        return ResponseEntity.created(uriOfNewResource).body(created);
    }

}
