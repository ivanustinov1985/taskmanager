package ru.volnenko.se;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.sql.Date;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 29.04.2020
 */
public class ProjectTestData {
    public static final String PROJECT_ONE_ID = "26f8ba1f-882c-11ea-8b4a-20cf3044b634";

    public static final Project FIRST_PROJECT =
            new Project(PROJECT_ONE_ID, "First project", Date.valueOf("2019-05-30"), Date.valueOf("2019-06-30"));
}
