package ru.volnenko.se;

import org.springframework.test.web.servlet.ResultMatcher;
import ru.volnenko.se.entity.Task;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.volnenko.se.TestUtil.readListFromJsonMvcResult;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 28.04.2020
 */
public class TaskTestData {
    public static final String TASK_ONE_ID = "402880e571ba849e0171ba95c19c0000";
    public static final String TASK_TWO_ID = "402880e571ba9a530171ba9dddf90000";
    public static final String TASK_THREE_ID = "402880e571bb7d710171bb8613900000";



    public static final Task TASK_ONE_FIRST_PROJECT =
            new Task(TASK_ONE_ID, "Task_FP", Date.valueOf("2020-04-26"), Date.valueOf("2020-04-30"));
    public static final Task TASK_TWO_FIRST_PROJECT =
            new Task(TASK_TWO_ID, "Task_FP", Date.valueOf("2020-04-26"), Date.valueOf("2020-04-29"));
    public static final Task TASK_ONE_SECOND_PROJECT =
            new Task(TASK_THREE_ID, "Task_SP", Date.valueOf("2020-04-02"), Date.valueOf("2020-04-05"));

    static {
        TASK_ONE_FIRST_PROJECT.setProject(ProjectTestData.FIRST_PROJECT);
        TASK_TWO_FIRST_PROJECT.setProject(ProjectTestData.FIRST_PROJECT);
    }

    public static final List<Task> TASKS = Arrays.asList(TASK_ONE_FIRST_PROJECT, TASK_TWO_FIRST_PROJECT, TASK_ONE_SECOND_PROJECT);

    public static Task getCreated() {
        return new Task(null, "NewTask", Date.valueOf("2020-04-26"), Date.valueOf("2020-04-30"));
    }

    public static Task getUpdated() {
        return new Task(TASK_ONE_FIRST_PROJECT.getId(), "UpdatedTask", Date.valueOf("2020-04-26"), Date.valueOf("2020-04-30"));
    }

    public static void assertMatch(Task actual, Task expected) {
        assertThat(actual).isEqualToComparingFieldByField(expected);
    }

    public static void assertMatch(Iterable<Task> actual, Task... expected) {
        assertMatch(actual, Arrays.asList(expected));
    }

    public static void assertMatch(Iterable<Task> actual, Iterable<Task> expected) {
        assertThat(actual).usingRecursiveFieldByFieldElementComparator().isEqualTo(expected);
    }

    public static ResultMatcher contentJson(Task... expected) {
        return contentJson(Arrays.asList(expected));
    }

    public static ResultMatcher contentJson(Iterable<Task> expected) {
        return result -> assertThat(readListFromJsonMvcResult(result, Task.class)).isEqualTo(expected);
    }
}
