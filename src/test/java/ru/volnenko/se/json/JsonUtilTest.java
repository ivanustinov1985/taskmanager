package ru.volnenko.se.json;

import org.junit.jupiter.api.Test;
import ru.volnenko.se.entity.Task;

import java.util.List;

import static ru.volnenko.se.TaskTestData.*;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 29.04.2020
 */
class JsonUtilTest {
    @Test
    void readWriteValue() throws Exception {
        String json = JsonUtil.writeValue(TASK_ONE_FIRST_PROJECT);
        System.out.println(json);
        Task task = JsonUtil.readValue(json, Task.class);
        System.out.println(task);
        assertMatch(task, TASK_ONE_FIRST_PROJECT);
    }

    @Test
    void readWriteValues() throws Exception {
        String json = JsonUtil.writeValue(TASKS);
        System.out.println(json);
        List<Task> tasks = JsonUtil.readValues(json, Task.class);
        assertMatch(tasks, TASKS);
    }

//    @Test
//    void testWriteOnlyAccess() throws Exception {
//        String json = JsonUtil.writeValue(UserTestData.USER);
//        System.out.println(json);
//        assertThat(json, not(containsString("password")));
//        String jsonWithPass = UserTestData.jsonWithPassword(UserTestData.USER, "newPass");
//        System.out.println(jsonWithPass);
//        User user = JsonUtil.readValue(jsonWithPass, User.class);
//        assertEquals(user.getPassword(), "newPass");
//    }
}