package ru.volnenko.se.controller.task;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.volnenko.se.controller.AbstractControllerTest;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.json.JsonUtil;
import ru.volnenko.se.service.TaskService;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.volnenko.se.TaskTestData.*;
import static ru.volnenko.se.TestUtil.readFromJson;
import static ru.volnenko.se.TestUtil.readFromJsonMvcResult;

/**
 * //TODO add comments.
 *
 * @author Ivan Ustinov(ivanustinov1985@yandex.ru)
 * @version 1.0
 * @since 29.04.2020
 */
class RestTaskControllerTest extends AbstractControllerTest {
    private static final String REST_URL = RestTaskController.REST_URL + "/";

    @Autowired
    private TaskService service;

    @Test
    void get() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(REST_URL + TASK_THREE_ID))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(readFromJsonMvcResult(result, Task.class), TASK_ONE_SECOND_PROJECT));
    }

    @Test
    void delete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(REST_URL + TASK_ONE_ID))
                .andExpect(status().isNoContent());
        assertMatch(service.getListTask(), TASK_TWO_FIRST_PROJECT, TASK_ONE_SECOND_PROJECT);
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(REST_URL))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(TASK_ONE_FIRST_PROJECT, TASK_TWO_FIRST_PROJECT, TASK_ONE_SECOND_PROJECT));
    }

    @Test
    void getProjectTasks() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(REST_URL + "projectTasks")
                .param("projectId", "26f8ba1f-882c-11ea-8b4a-20cf3044b634"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((contentJson(TASK_ONE_FIRST_PROJECT, TASK_TWO_FIRST_PROJECT)));
    }

    @Test
    void update() throws Exception {
        Task updated = getUpdated();
        mockMvc.perform(MockMvcRequestBuilders.put(REST_URL + TASK_ONE_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(updated)))
                .andExpect(status().isNoContent());
        assertMatch(service.getTaskById(TASK_ONE_ID), updated);
    }

    @Test
    void createWithLocation() throws Exception {
        Task created = getCreated();
        ResultActions action = mockMvc.perform(MockMvcRequestBuilders.post(REST_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(created))
                .param("projectId", "null")
        );
        System.out.println(JsonUtil.writeValue(created));

        Task returned = readFromJson(action, Task.class);
        created.setId(returned.getId());

        assertMatch(returned, created);
        assertMatch(service.getListTask(), TASK_ONE_FIRST_PROJECT, TASK_TWO_FIRST_PROJECT, TASK_ONE_SECOND_PROJECT, created);
    }
}